---
description: Overview of BEPSwap.
---

# BEPSwap Overview

## Overview

BEPSwap is comprised of a Layer-2 statechain that monitors for events relating to an asset pool on Binance Chain, then comes to consensus on these observations.

Once observation-consensus is reached on a transaction, then business logic is applied which invokes a state change in the statechain. 

The output of the state change typically is also comprised of a transaction, such as a `swap`, `refund` or `withdrawal`. However, sometimes it does not, in the case of `stake` and `admin` transactions, which just updates state in the statechain with no related transaction. 

The details of this transaction are then passed to a Threshold-Signature-Signing \(TSS\) module, which then signs and broadcasts this to Binance Chain. The outgoing transaction is then observed once again to close the loop on the transaction. 

As such, each validator on the statechain is an **observer-validator-signer** that does three distinct things:

1. Observes Binance Chain and makes observation transactions into the Statechain
2. Validates the Statechain blocks through tendermint-based consensus
3. Signs outgoing TSS transactions with a Binance Chain signer key.

### Keys

They also hold four keys which all need to be whitelisted by the protocol: 

<table>
  <thead>
    <tr>
      <th style="text-align:left">Key</th>
      <th style="text-align:left">Chain</th>
      <th style="text-align:left">Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left"><b>OBSERVER</b> Key</td>
      <td style="text-align:left">BEPSwap Statechain</td>
      <td style="text-align:left">
        <p>Posts observation transactions into the statechain.</p>
        <p>Stored on disk (online).</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>VALIDATOR</b> Key</td>
      <td style="text-align:left">BEPSwap Statechain</td>
      <td style="text-align:left">
        <p>Signs statechain blocks.</p>
        <p>Stored on disk (online).</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>SIGNER</b> Key</td>
      <td style="text-align:left">Binance Chain</td>
      <td style="text-align:left">
        <p>Signs outgoing transactions.</p>
        <p>Stored on disk (online).</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>ADMIN</b> Key</td>
      <td style="text-align:left">Binance Chain</td>
      <td style="text-align:left">
        <p>Makes <b>ADMIN</b> transactions to the pool address.</p>
        <p>Stored offline.</p>
      </td>
    </tr>
  </tbody>
</table>{% hint style="info" %}
 The statechain is a Cosmos-SDK powered blockchain that does not peg assets or use any native assets. Instead, it only maintains state that originates from Binance Chain and applies logic to this state. Validators are whitelisted initially via Proof-of-Authority. 
{% endhint %}

### Observer

Each Service Node has an **observer** which scans Binance Chain to look for events relating to the pool address. Each time it discovers an event it posts it into the statechain. 

Read more here:

{% page-ref page="observer.md" %}

### Validator

Each Service Node has a **validator** which proposes and commits blocks to the statechain. The blocks contain state changes made as part of the CosmosSDK application logic. 

Read more here: 

{% page-ref page="validator.md" %}

### Signer

Each Service Node has a **signer** which queries for outgoing transactions and signs them using a Threshold-Signature-Signing scheme based on Genarro-Goldfeder. 

Read more here: 

{% page-ref page="signer.md" %}

### Security

All three parts of the Service Node has byzantine fault tolerance and a clear separation of concerns. Additionally, it can be seen that the only state that can be passed into the statechain is from validated & finalised Binance Chain transactions to and from the asset pool addresses, which significantly reduces the attack vectors. 

Read more here:

{% page-ref page="security.md" %}

### Chain Service

A Chain Service is maintained which scans the statechain for events and populates an external, easily queried database which can be used by developers and third-party services.

Read more here:

{% page-ref page="chain-service.md" %}

### Clients

Third-party clients integrate the BEPSwap services in order to facilitate swapping and staking. Clients also need to integrate the Binance Chain SDK in order to make transactions to BEPSwap. 

Read more here:

