# Table of contents

* [BEPSwap Overview](README.md)
* [Definitions](definitions.md)
* [Observer](observer.md)
* [Validator](validator.md)
* [Signer](signer.md)
* [Security](security.md)
* [Chain Service](chain-service.md)

## BEPSwap Liquidity Design

* [Continuous Liquidity Pools](bepswap-liquidity-design/continuous-liquidity-pools.md)
* [Staking](bepswap-liquidity-design/staking.md)
* [Fees](bepswap-liquidity-design/fees.md)

## Integration Guide

* [Overview](integration-guide/overview.md)
* [Connecting](integration-guide/connecting.md)
* [Making Transactions](integration-guide/making-transactions.md)
* [Admin Transactions](integration-guide/admin-transactions.md)
* [MEMO Alternatives](integration-guide/memo-alternatives.md)

## Service Nodes

* [Overview](service-nodes/overview.md)
* [Config](service-nodes/config.md)
* [Start-up](service-nodes/start-up.md)
* [Duty Cycle](service-nodes/duty-cycle.md)
* [Security](service-nodes/security.md)

