# Connecting

## Overview

Clients will be connecting to three separate sources of information:

<table>
  <thead>
    <tr>
      <th style="text-align:left">Source</th>
      <th style="text-align:left">Notes</th>
      <th style="text-align:left">Base URL</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">Binance Chain</td>
      <td style="text-align:left">
        <p><b>Mandatory</b>.</p>
        <p>Query account balances, make transactions.</p>
      </td>
      <td style="text-align:left"><a href="https://dex.binance.org/api/v1/">https://dex.binance.org/api/v1/</a>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">BEPSwap Statechain</td>
      <td style="text-align:left">
        <p><b>Mandatory</b>.</p>
        <p>Query pool address and balances. Compose transaction MEMOs</p>
      </td>
      <td style="text-align:left"><a href="https://chain.bepswap.com/">https://chain.bepswap.com/</a>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">BEPSwap Chain Service</td>
      <td style="text-align:left">
        <p><b>Optional.</b>
        </p>
        <p>Query for pool, swap, stake, admin, user and network data</p>
      </td>
      <td style="text-align:left"><a href="https://api.bepswap.com/">https://api.bepswap.com/</a>
      </td>
    </tr>
  </tbody>
</table>### Binance Chain

To make a transaction, clients should first connect with Binance Chain via the SDK:

{% embed url="https://docs.binance.org/api-reference/sdk.html" %}

For simple **web-based** clients, the [Javascript SDK](https://github.com/binance-chain/javascript-sdk) is sufficient. For **bots** and **automated scripts**, use the [Python](https://github.com/binance-chain/python-sdk) or [Go](https://github.com/binance-chain/go-sdk) versions. 

### Statechain

The client then should connect to the Statechain API:

{% embed url="https://chain.bepswap.com" %}

The client then should source the following information:

| Info | Endpoint |
| :--- | :--- |
| Supported Pools | {{statechain}}/pools |
| Rune Ticker | {{statechain}}/rune |
| Pool Balances | {{statechain}}/poolData?asset=LOK-3C0 |

### Chain Service

The client can also connect to the Chain Service in order to display supporting metrics.

{% embed url="https://api.bepswap.com/" %}

