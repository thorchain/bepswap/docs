---
description: Alternative forms of MEMOs are available
---

# MEMO Alternatives

### MEMO Alternatives

Alternative MEMO types are available to clients:

| Transaction | Long-form MEMO | Short-form MEMO | Symbol MEMO |
| :--- | :--- | :--- | :--- |
| CREATE | `CREATE` | `c` | `?` |
| STAKE | `STAKE` | `st` | `+` |
| WITHDRAW | `WITHDRAW` | `wd` | `-` |
| SWAP | `SWAP` | `s` | `=` |
| GAS | `GAS` | `g` | `$` |
| ADD | `ADD` | `a` | `%` |
| ADMIN | `ADMIN` | `n` | `!` |
| POOL | `OUTBOUND` | `out` | `#` |



