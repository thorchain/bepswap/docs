---
description: Admins can make changes to the Statechain
---

# Admin Transactions

### Overview

Each BEPSwap Validator has an **ADMIN** and **SIGNER** account which is whitelisted and allows them to suggest changes to the statechain global parameters. 

Changes are finalised as long as 67% of the active validators cast identical transactions. Admin transactions don't expire and multiple votes from the same **ADMIN** account don't add up. 

There are also **POOL** transactions that are part of BEPSwap operating, which are the only **outbound** transactions from the pool. 

### ADMIN Transactions

The following transactions are permissible from ADMIN accounts:

<table>
  <thead>
    <tr>
      <th style="text-align:left">Type</th>
      <th style="text-align:left">Payload</th>
      <th style="text-align:left">MEMO</th>
      <th style="text-align:left">FROM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">POOL STATUS</td>
      <td style="text-align:left"><code>0.00000001 BNB</code> 
      </td>
      <td style="text-align:left">
        <p><code>ADMIN:SYMBOL:STATUS</code>
        </p>
        <p></p>
        <p>Where Status can be:</p>
        <p><code>ENABLE</code>: Enable the pool.
          <br /><code>BOOTSTRAP</code>: Put the pool in bootstrap mode.</p>
        <p><code>DISABLE</code>: Disable the pool.</p>
        <p><code>SUSPEND</code>: Suspend the pool.</p>
      </td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">WHITELIST</td>
      <td style="text-align:left"><code>0.00000001 BNB</code> 
      </td>
      <td style="text-align:left">
        <p><code>ADMIN:WHITELIST:ADDR</code>
        </p>
        <p></p>
        <p>The address to whitelist is added.</p>
      </td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Global Slip Limit</td>
      <td style="text-align:left"><code>0.00000001 BNB</code> 
      </td>
      <td style="text-align:left"><code>ADMIN:GSL:XXX</code>
        <br />
        <br />Where <code>XXX</code> is 0-100 for % limit.</td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Trade Slip Limit</td>
      <td style="text-align:left"><code>0.00000001 BNB</code> 
      </td>
      <td style="text-align:left"><code>ADMIN:TSL:XXX</code>
        <br />
        <br />Where <code>XXX</code> is 0-100 for % limit.</td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Whitelist Key</td>
      <td style="text-align:left"><code>0.00000001 BNB</code>
      </td>
      <td style="text-align:left">
        <p><code>ADMIN:WL:KEY:ADDR</code>
          <br />
          <br />Where <code>KEY</code> is:</p>
        <p><code>O</code> Observer</p>
        <p><code>S</code> Signer</p>
        <p><code>V</code> Validator</p>
        <p><code>A</code> Admin</p>
        <p></p>
        <p>And the address is the address to be whitelisted.</p>
      </td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Remove Key</td>
      <td style="text-align:left"><code>0.00000001 BNB</code>
      </td>
      <td style="text-align:left">
        <p><code>ADMIN:RM:KEY:ADDR</code>
          <br />
          <br />Where <code>KEY</code> is:</p>
        <p><code>O</code> Observer</p>
        <p><code>S</code> Signer</p>
        <p><code>V</code> Validator</p>
        <p><code>A</code> Admin</p>
        <p></p>
        <p>And the address is the address to be removed.</p>
      </td>
      <td style="text-align:left"><b>ADMIN</b>
      </td>
    </tr>
  </tbody>
</table>### SIGNER Transactions

Validators also have a **SIGNER** account that can also perform certain transactions, in the case of updating the pool address each duty cycle. Since the Validator has to prove ownership of the **SIGNER** account in making the transaction, it ensures they have ownership of the private key needed to sign transactions.

<table>
  <thead>
    <tr>
      <th style="text-align:left">Type</th>
      <th style="text-align:left">Payload</th>
      <th style="text-align:left">MEMO</th>
      <th style="text-align:left">FROM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">POOL ADDRESS</td>
      <td style="text-align:left"><code>0.00000001 BNB</code> 
      </td>
      <td style="text-align:left">
        <p><code>SIGNER:POOL:ADDR</code>
        </p>
        <p></p>
        <p>Where the new pool address is specified.</p>
      </td>
      <td style="text-align:left"><b>SIGNER</b>
      </td>
    </tr>
  </tbody>
</table>### POOL Transactions

Transactions that are successfully compiled, signed and committed to Binance Chain as part of BEPSwap are identified by Statechain Blockheight and observed. They are logged in order to provide an audit trail of a successful outbound transaction. 

<table>
  <thead>
    <tr>
      <th style="text-align:left">Type</th>
      <th style="text-align:left">Payload</th>
      <th style="text-align:left">MEMO</th>
      <th style="text-align:left">FROM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">OUTBOUND</td>
      <td style="text-align:left"><code>txArray</code> 
      </td>
      <td style="text-align:left">
        <p><code>OUTBOUND:STATECHAIN-BLOCKHEIGHT</code>
        </p>
        <p></p>
        <p>Outbound transactions from the pool address.</p>
      </td>
      <td style="text-align:left"><b>POOL</b>
      </td>
    </tr>
  </tbody>
</table>