# Making Transactions

### Overview

The BEPSwap statechain processes all transactions made to the pool address on Binance Chain that it monitors. The pool address is discovered by clients by querying the statechain. 

Transactions to the BEPSwap pool pass user-intent with the `MEMO` field on Binance Chain. The BEPSwap statechain inspects the transaction object, as well as the `MEMO` in order to process the transaction, so care must be taken to ensure the `MEMO` and the transaction is valid. If not, the statechain will automatically refund. 

### Transactions

The following transactions are available to anyone:

<table>
  <thead>
    <tr>
      <th style="text-align:left">Type</th>
      <th style="text-align:left">Payload</th>
      <th style="text-align:left">MEMO</th>
      <th style="text-align:left">Expected Outcome</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">CREATE POOL</td>
      <td style="text-align:left">
        <p><code>RUNE &amp;&amp; Token</code>
        </p>
        <p>Must contain both.</p>
      </td>
      <td style="text-align:left">
        <p><code>CREATE:SYMBOL</code>
        </p>
        <p>Symbol is set by Binance Chain</p>
      </td>
      <td style="text-align:left">Creates a new pool</td>
    </tr>
    <tr>
      <td style="text-align:left">STAKE</td>
      <td style="text-align:left">
        <p><code>RUNE &amp;| Token</code>
        </p>
        <p>Can be either, or just one side.</p>
      </td>
      <td style="text-align:left">
        <p><code>STAKE:SYMBOL</code>
        </p>
        <p>Symbol is set by Binance Chain</p>
      </td>
      <td style="text-align:left">Stakes into the specified pool.</td>
    </tr>
    <tr>
      <td style="text-align:left">WITHDRAW</td>
      <td style="text-align:left">
        <p><code>0.00000001 BNB</code>
        </p>
        <p>A non-zero transaction.</p>
      </td>
      <td style="text-align:left">
        <p><code>WITHDRAW:SYMBOL:PERCENT</code>
        </p>
        <p>Percent is in basis points (0-10000, where 10000=100%)</p>
      </td>
      <td style="text-align:left">Withdraws from a pool</td>
    </tr>
    <tr>
      <td style="text-align:left">SWAP</td>
      <td style="text-align:left">
        <p><code>RUNE || Token</code>
        </p>
        <p>Either/Or</p>
      </td>
      <td style="text-align:left">
        <p><code>SWAP:SYMBOL:DESTADDR:LIM</code>
        </p>
        <p>Set a destination address to swap and send to someone. If <code>DESTADDR</code>is
          blank, then it sends back to self:</p>
        <p><code>SWAP:SYMBOL::LIM</code>
          <br />
        </p>
        <p>Set price protection to prevent front-running. If the price isn&apos;t
          achieved<code>+/- tradePriceLimit</code> then it is refunded.</p>
        <p>If <code>LIM</code> is ommitted, then there is no price protection:</p>
        <p> <code>SWAP:SYMBOL:DESTADDR</code>
        </p>
        <p></p>
        <p>If both are ommitted then the format is:</p>
        <p> <code>SWAP:SYMBOL</code>
        </p>
      </td>
      <td style="text-align:left">Swaps to token.</td>
    </tr>
    <tr>
      <td style="text-align:left">GAS</td>
      <td style="text-align:left"><code>BNB</code>
      </td>
      <td style="text-align:left"><code>GAS</code>
      </td>
      <td style="text-align:left">BNB is added to the gas balance</td>
    </tr>
    <tr>
      <td style="text-align:left">ADD Assets</td>
      <td style="text-align:left">
        <p><code>RUNE &amp;| Token</code>
        </p>
        <p>Can be either, or just one side.</p>
      </td>
      <td style="text-align:left"><code>ADD:SYMBOL</code>
      </td>
      <td style="text-align:left">Adds to the pool balances.</td>
    </tr>
  </tbody>
</table>### Refunds

The following are the conditions for refunds:

| Condition | Notes |
| :--- | :--- |
| Invalid `MEMO` | If the `MEMO` is incorrect the user will be refunded. |
| Invalid Assets | If the asset for the transaction is incorrect \(staking an asset into a wrong pool\) the user will be refunded. |
| Invalid Transaction Type | If the user is performing a multi-send vs a send for a particular transaction, they are refunded. |
| Exceeding Global Slip Limit | If a trade exceecds the global slip limit, they are refunded.  |
| Exceeding Trade Price Limit | If the final price achieved in a trade differs to expected, they are refunded.  |

Refunds cost fees to prevent Denial of Service attacks:

| Asset | Amount |
| :--- | :--- |
| RUNE | 1 RUNE |
| Non-RUNE Asset | 1 RUNE equivalent |



